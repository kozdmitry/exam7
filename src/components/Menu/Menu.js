import React from 'react';

const Menu = props => {
    return (
        <div>
            <b className={props.name}></b>
            <p className={props.price}></p>
            <span className={props.count}></span>
        </div>
    );
};

export default Menu;