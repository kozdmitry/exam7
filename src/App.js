import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './App.css';
import {Container, Grid} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Menu from "./components/Menu/Menu";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    }
}));

const App = () => {
    const classes = useStyles();
    const [items, setItems] = useState([
        {id: nanoid(), name: 'Hamburger', price: 80, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'},
        {id: nanoid(), name: 'Coffee', price: 70, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'},
        {id: nanoid(), name: 'Cheeseburger', price: 90, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'},
        {id: nanoid(), name: 'Tea', price: 20, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'},
        {id: nanoid(), name: 'Fries', price: 45, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'},
        {id: nanoid(), name: 'Pepsi', price: 35, count: 0, img: 'https://www.svgrepo.com/show/42950/fast-food.svg'}
    ]);

    const countAdd = id => {
        const index = items.findIndex(i => i.id === id);
        const itemsCopy = [...items];
        const obj = itemsCopy[index];
        obj.count++;
        setItems(itemsCopy);
    };

    const totalPrice = () => {
        return items.reduce((acc, item) => {
            if (item.count > 0) {
                return acc + (item.price * item.count);
            }
            return acc;
        }, 0)
    };

    const showMenu = name => {
        const arr = [];
        const index = items.findIndex(i => i.name === name)
        const itemsCopy = [...items];
        const ing = itemsCopy[index];
        const count = ing.count;

        for (let i = 0; i < count; i++) {
            arr.push(
                <Menu
                    key={arr.length}
                    name={ing.name}
                    price={ing.price}
                    count={ing.count}
                />
            );
        }
        console.log(arr)
        return arr;
    }

    let item;

    return (
        <Container maxWidth="md" className={classes.root}>
            <CssBaseline/>
            <Grid container spacing={2}>
                {items.map(item => (
                    <Grid item key={item.id}>
                        <Paper component={Box} p={2}>
                            <Grid item container alignItems="center" justify="space-between">
                                <button onClick={() => countAdd(item.id)}>
                                    <Grid item>
                                        <h4>{item.name}</h4>
                                        <p>Price: {item.price} KGS</p>
                                        <img className="img" src={item.img}/>
                                    </Grid>
                                </button>
                            </Grid>
                        </Paper>
                    </Grid>
                ))};
            </Grid>
            <Paper component={Box} p={3} mt={1}>
                <div className="Order">
                    {showMenu("Hamburger")}
                    {showMenu("Coffee")}
                    {showMenu("Cheeseburger")}
                    {showMenu("Tea")}
                    {showMenu("Fries")}
                    {showMenu("Pepsi")}
                    <strong>Total price: {totalPrice()} KGS</strong>
                </div>
            </Paper>
        </Container>
    );
};

export default App;
